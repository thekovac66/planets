let planetsData;

const getPlanetsData = async () => {
    if (planetsData) {
        return new Promise((resolve, reject) => resolve(planetsData));
    }

    const response = await fetch('data.json', {
        method: 'GET',
        headers: {
            Accept: 'application/json'
        }
    });
    planetsData = response.json();
    return await planetsData;
};
const getCurrentRoute = () => {
    return window.location.hash;
};

const imageMapping = {
    overview: 'planet',
    structure: 'internal',
    geology: 'planet'
};

const setTabsState = (planetName, planetInfoTab) => {
    const overviewTabLink = document.getElementById('overview-tab-link');
    const structureTabLink = document.getElementById('structure-tab-link');
    const surfaceTabLink = document.getElementById('geology-tab-link');
    const overviewSideTabLink = document.getElementById('overview-side-tab-link');
    const structureSideTabLink = document.getElementById('structure-side-tab-link');
    const surfaceSideTabLink = document.getElementById('geology-side-tab-link');
    overviewTabLink.href = `#/${planetName}/overview`;
    structureTabLink.href = `#/${planetName}/structure`;
    surfaceTabLink.href = `#/${planetName}/geology`;
    overviewSideTabLink.href = `#/${planetName}/overview`;
    structureSideTabLink.href = `#/${planetName}/structure`;
    surfaceSideTabLink.href = `#/${planetName}/geology`;
    const activeTab = document.getElementById(`${planetInfoTab}-tab-link`);
    const activeSideTab = document.getElementById(`${planetInfoTab}-side-tab-link`);
    const previousTabs = document.getElementsByClassName('active-tab');
    if (previousTabs.length) {
        Array.from(previousTabs).forEach((element) => {
            element.classList.remove('active-tab');
        });
    }
    activeSideTab.classList.add('active-tab');
    activeTab.classList.add('active-tab');
};

const setPlanetSegments = (planet, planetInfoTab) => {
    const planetNameDiv = document.getElementById('planet-name');
    const planetInfo = document.getElementById('planet-info-text');
    const planetSourceLink = document.getElementById('planet-source-link');
    const planetRotation = document.getElementById('planet-rotation');
    const planetRevolution = document.getElementById('planet-revolution');
    const planetRadius = document.getElementById('planet-radius');
    const planetTemp = document.getElementById('planet-temp');

    planetNameDiv.textContent = planet.name;
    planetInfo.textContent = planet[planetInfoTab].content;
    planetSourceLink.href = planet[planetInfoTab].source;
    planetRotation.textContent = planet.rotation;
    planetRevolution.textContent = planet.revolution;
    planetRadius.textContent = planet.radius;
    planetTemp.textContent = planet.temperature;
    setPlanetImages(planet, planetInfoTab);
};

const setPlanetImages = (planet, planetInfoTab) => {
    const planetImage = document.getElementById('planet-image');
    const overlayImage = document.getElementById('overlay-image');
    planetImage.src = planet.images[imageMapping[planetInfoTab]];
    planetImage.className = `${planet.name.toLowerCase()}-image`;
    if (planetInfoTab === 'geology') {
        overlayImage.src = planet.images.geology;
        overlayImage.classList.add('show');
    } else {
        overlayImage.classList.remove('show');
    }
};

const setCssVariables = (planet) => {
    var root = document.querySelector(':root');
    root.style.setProperty('--active-planet-color', planet.color);
};

const setNavigationStyling = (planetName) => {
    const previousActive = document.getElementsByClassName('active-planet');
    if (previousActive.length) {
        previousActive[0].className = '';
    }
    const planetNavItem = document.getElementById(`planet-${planetName}`);

    planetNavItem.className = 'active-planet';
};
const handleLocation = async () => {
    if (!window.location.hash) {
        window.history.pushState({}, '', '#/mercury/overview');
    }
    const planets = await getPlanetsData();
    const currentRoute = getCurrentRoute();
    const routeSegments = currentRoute.substring(2).split('/');
    const planetName = routeSegments[0];
    const planetInfoTab = routeSegments.length === 2 ? routeSegments[1] : 'overview';
    const planet = planets.find((planet) => planet.name.toLowerCase() === planetName);

    setPlanetSegments(planet, planetInfoTab);
    setTabsState(planetName, planetInfoTab);
    setCssVariables(planet);
    setNavigationStyling(planetName);
    toggleMobileNavigation(true);
};

const toggleMobileNavigation = (isLocationChange) => {
    const navigation = document.getElementById('planet-navigation');
    const header = document.getElementById('header');
    if (!navigation.classList.contains('show') && !isLocationChange) {
        navigation.classList.add('show');
        header.classList.add('show');
        return;
    }
    navigation.classList.remove('show');
    header.classList.remove('show');
};
window.toggleMobileNavigation = toggleMobileNavigation;
window.onpopstate = handleLocation;
handleLocation();
